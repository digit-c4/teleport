FROM debian:latest

EXPOSE 3022
EXPOSE 3025
EXPOSE 3080 

RUN \
  apt-get update \
  && apt-get -y install procps curl gettext-base \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/teleport
RUN mkdir /var/lib/teleport

RUN curl https://goteleport.com/static/install.sh | bash -s 15.3.7

COPY templates/teleport.yaml.template /etc/teleport/teleport.yaml.template
COPY templates/snet-access.yaml.template /etc/teleport/snet-access.yaml.template
COPY templates/user-teleport-admin.yaml.template /etc/teleport/user-teleport-admin.yaml.template
COPY templates/user-teleport-auditor.yaml.template /etc/teleport/user-teleport-auditor.yaml.template
COPY templates/user-teleport-user.yaml.template /etc/teleport/user-teleport-user.yaml.template

ENV CLUSTERNAME Teleport_Cluster
ENV IP 127.0.0.1
ENV LOG_LEVEL ERROR 
ENV TWOFA off
ENV ADMIN_PASSWORD JDJhJDEwJHJveGZLUFpsbW54NXhjWW9US09rRC5aWFJ6eVE2bTN0NUM2eWpZT3oubm1oeVhNMzBUTFNh
ENV USER_PASSWORD JDJhJDEwJHJveGZLUFpsbW54NXhjWW9US09rRC5aWFJ6eVE2bTN0NUM2eWpZT3oubm1oeVhNMzBUTFNh
ENV AUDITOR_PASSWORD JDJhJDEwJHJveGZLUFpsbW54NXhjWW9US09rRC5aWFJ6eVE2bTN0NUM2eWpZT3oubm1oeVhNMzBUTFNh
ENV ADMIN_TOTP ABCDEFGHIJKLMNOPQRSTUVWXYZ
ENV USER_TOTP ABCDEFGHIJKLMNOPQRSTUVWXYZ
ENV AUDITOR_TOTP ABCDEFGHIJKLMNOPQRSTUVWXYZ

ENTRYPOINT ["/bin/bash",  "-c", "echo '127.0.0.1 teleport-vip.ec.local' >> /etc/hosts && envsubst < /etc/teleport/snet-access.yaml.template > /etc/teleport/snet-access.yaml && envsubst < /etc/teleport/user-teleport-admin.yaml.template > /etc/teleport/user-teleport-admin.yaml && envsubst < /etc/teleport/user-teleport-auditor.yaml.template > /etc/teleport/user-teleport-auditor.yaml && envsubst < /etc/teleport/user-teleport-user.yaml.template > /etc/teleport/user-teleport-user.yaml && envsubst < /etc/teleport/teleport.yaml.template > /etc/teleport/teleport.yaml && /usr/local/bin/teleport start -c /etc/teleport/teleport.yaml"]

