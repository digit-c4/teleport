#!/bin/bash
PASS=`openssl rand -hex 32`
OTPSEED=`openssl rand 20 | base32`
TELEPORT_PASS=`htpasswd  -nb -B -C 10  user ${PASS}| head -1 | cut -f 2 -d ":"| base64 -w 0`
echo "PASS: ${PASS}"
echo "TELEPORT_PASS: ${TELEPORT_PASS}"
echo "OTP: ${OTPSEED}"
