# teleport

[teleport](https://goteleport.com/) is an easy and secure way to access all the infrastructure

# Getting started

## Requirements

To use teleport we use netbox to store settings and configuration.
You must have a netbox somewhere configured with teleport service setup.

You also needs the following components :
- ansible
- docker

## For Developpers

### Installation and Setup

```
git checkout https://code.europa.eu/digit-c4/teleport.git
ansible-galaxy collection install -r collections/requirements.yml
export NETBOX_API="http://latest.netbox.ntx.lu"
export NETBOX_TOKEN="5b79e163fd2264140e32223b53f4efedee91ec15"
docker pull code.europa.eu:4567/digit-c4/teleport/one-ssh
```

### Test and Deploy

```
ansible-playbook ansible/deploy.yml -i ansible/netbox-local.yml
```

### Integration

- [Netbox data](https://code.europa.eu/digit-c4/netbox/-/tree/main/ansible/playbooks?ref_type=heads)
- [Awx data](https://code.europa.eu/digit-c4/awx-data/-/tree/main/ansible?ref_type=heads)

### Contribute

to contribute to our repository please make sure you have read and understood the following references :
- [Contributing](./CONTRIBUTE.md)
- [Code of Conduct](./CODE_OF_CONDUCT.md)

## For Users

```
ansible-playbook ansible/deploy.yml -i ansible/netbox-local.yml
```

## For Docker Nerds

```
docker run -e IP=0.0.0.0 -v teleport_config:/etc/teleport -v teleport_data:/var/lib/teleport -p 3022:3022 -p 3080:3080 -p 3025:3025 --name teleport teleport
```

## Support

### Issues
Any problem please open an [issue](https://code.europa.eu/digit-c4/teleport/-/issues)

### Apidoc
You can find the latest version of openapi [here](https://digit-c4.pages.code.europa.eu/teleport/)

## The Team
[Robi](https://code.europa.eu/hendrrt) PO

[Niol](https://code.europa.eu/nicolov) devlead

[Lav](https://code.europa.eu/veniela) PPO

## License

Please refer to the [LICENCE](./LICENSE) document
