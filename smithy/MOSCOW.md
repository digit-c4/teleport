# Core features (Functional requirements)

| Access                                         | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|
| SSH bastion                                       |    | Must         | &check; | &check;  | &check; |  ||
| Reverse SSH bastion (NAT)                         |    | Could        | &check; | -        | Multi-hop &check; ENT license  |||
| TCP bastion (database)                            |    | Should       | &check; | -        | &check; |||
| HTTP bastion                                      |    | Could        | &check; | -        |         |||
| RDP                                               |    | Should       | &check; | &check;  |         |||
| SSH bastion for linux host (ephem key)            |    | Must         | &check; | &cross;  |         |||
| SSH bastion for linux host (ephem cert)           |    | Must         | &check; | &check;  |         |||
| SSH bastion for appliance device (pass)           |    | Should       | &cross; | -        |         |||
| Credential injection from vault [Hashicorp only]  |    | Nice to have | N/A     | N/A      | &check; ENT license |||
| Shared session                                    |    | Nice to have | &check; | -        |         |||
| File transfer                                     |    | Nice to have | &check; | &check;  |         |||
| SSH port forward                                  |    | Nice to have | &check; | -        |         |||
| Web-based access                                  |    | Must         | &check; | &check;  |         |||
| openssh client  access                            |    | Must         | &check; | &check;  |         |||
| Common Windows (Putty/MobaXterm) clients access   |    | Must         | &check; | -        |         |||
| Proprietary client Windows                        |    | Nice to have | &check; | -        | &check; |||
| Proprietary client Linux                          |    | Nice to have | &check; | -        | &check; |||
| Proprietary client MacOS                          |    | Nice to have | &check; | -        | &check; |||
| Machine2machine                                   |    | Nice to have | &check; | -        |         |||
| Access via HTTP proxy                             |    | Should       | &check; | -        | https_proxy |||

| Recordings                                         | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|
| SSH session recording                 |    | Could         | &check; | &check; |
| Web session recording                 |    | Nice to have  | &check; | &check; | &check; ENT license |
| RDP session recording                 |    | Nice to have  | &check; | &check; |
| Database queries recording            |    | Nice to have  | &check; | &check; |
| Send recordings to external system    |    | Nice to have  | &check; | -       |


# Integration features (non-functional requirements)

| AuthN/Z                                 | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|
| Local user management for last resort     |    | Must          | &check; | &check; | &check; |
| Local group management    |    | Must          | &check; | &check; | &check; |
| Password self management for last resort  |    | Nice to have  | &check; | -       | &check; |
| Local authentication for last resort      |    | Must          | &check; | &check; | &check; |
| Local password policy     | UAM 130, 131, 132, 133, 134, 135 | Nice to have  | &cross; | &check; | simple &check; 
| Local authentication + MFA|    | Should        | &check; | -       | &cross; |
| OIDC authentication       | UAM 123 | Should        | &check; | &check; | &check; |
| SAML authentication       | UAM 123 | Should        | &check; | &check; | &cross; |
| Token authentication      | UAM 123 | Nice to have  | &check; | -       |
| RBAC                      | UAM 119, 120 | Must          | &check; | &check; | &check; |
| Approval workflow         |    | Should        | &check; | &check; | &check; |
| Maximum session duration  |    | Nice to have  | &check; | -       | &check; |
| Encryption  | CRY 88 | Should  | &check; | -       | &check; |
| Certificate Validation | CRY 79 | Should  | &check; | -       | &check; |

| Logging                               | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|
| Access auditing                       | LAM 92, 94, 95 | Must          | &check; | &check; |
| Management auditing                   | LAM 92, 94, 95 | Must          | &check; | -       |
| Local log           | LAM 92, 94, 96, 98| Should        | &check; | &check; |
| Management activities                 | LAM 92, 94 | Must          | &check; | -       |
| Reporting                             | LAM 92, 94 | Nice to have  | &check; | -       |
| Splunk/SIEM integration               | LAM 99, 102, 103 | Nice to have  | &check; | &check; |


| Alerting                              | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|
| Brute force protection                |    | Nice to have    | &check; | &check; | &check; | 
| Password expiration                   |    | Nice to have    | &check; | &check; |

| Monitoring                        | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------------| ---------|---------|---------|-------------------|----------|----------|--------|--------| 
| Service metrics                       |    | Nice to have    | &check; | &check; | &check; |

| Deployment                         | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------| 
| Run on Linux VM                               |    | Should    | &check; | &check; | &check; |
| Containerized solution                        |  VUM 29 | Must      | &check; | [K8S 23 nodes!!][prxk8s] &cross;|  &check; |
| Native Agent                                  |    | Could     | &check; | &check; |   |
| Agentless                                     |    | Could     | &check; | &check; |
| High availability                             |    | Must      | &check; | External LB &check; | External LB/ PostrgreSQL / KMS |
| Host registration                             |    | Should    | &check; | from AWS/GCP/Azure/OpenStack/vSphere | from AWS/Azure |
| Admin API                                     |    | Should    | &check; | &check; |
| Admin CLI                                     |    | Could     | &check; | -       |
| Admin WebUI                                   |    | Should    | &check; | &check; |
| Zero touch deployment & initial configuration |    | Should    | &check; | -       |
| Cloud agnostic & onPrem                       |    | Must      | &check; | GCP/AWS/Azure/K8S &check; | &check; |
| Do not require SaaS component                 |    | Must      | &check; | &check; |  &check; |
| Deployment complexity                         |    | Must Not  | &cross; | &check; |
| Updates & Patching                            |    | Must      | &check; | -       | 


| Backup/Restore | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------|    
| Backup    |    | Should    | &check; | &check; |
| Restore   |    | Must      | &check; | &check; |

|Licensing & Suppport | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------| 
| Open source license                   |    | Nice to have      | &check; | -       | &check; |
| Free (as abeer) usage                 |    | Nice to have      | &cross; | -       | &cross; |
| Licenses per instance                 |    | Should Not        | &check; | -       | &cross; |
| Licenses per user/instance            |    | Should Not        | &check; | -       | &cross; |
| Licenses per user (on all instances)  |    | Not nice to have  | &check; | -       | &check; |
| Licenses per resource                 |    | Should Not        | &check; | -       | &cross; |
| Licenses per session                  |    | Should Not        | &cross; | -       | &check; |
| Direct support                        |    | Should            | &check; | -       | &check; ENT license|
| Partner support                       |    | Should            | &check; | -       | -       |
| Community support                     |    | Should            | &check; | -       | &check; |
| Vulnerability management support      | VUM 25, 27, 28 | Must            | &check; | -       | &check; |

| "Reputation" | Compliancy | Classification | [Teleport][tp] | [PrivX][prx] |[HCP Boundary][bou] | [strongDM][sdm] | [BeyondTrust][bey] |[Wallix][wal] | [Perimeter 81 Ztna][per] | [Axis Security][axi] | [Twingate][twn] | [JumpServer][jmp] |
| --------- | --------- | --------------- | ------- | ---------| ---------|-------------|--------|-------------------|----------|----------|--------|--------| 
| Active development            |    |    | &check; | &check; |
| Vulnerabilty history          | VUM 26 |    | &check; | &check; |
| Support quality               |    |    | &check; | &check; |
| Company/Community size        |    |    | &check; | &check; |
| Production adoption           |    |    | &check; | &check; |
| Alignment with Commission cybersecurity concerns on geopolitical independence | - |  &check; |

[tp]: https://goteleport.com/docs/
[twn]: https://www.twingate.com/docs
[sdm]: https://www.strongdm.com/docs
[bey]: https://www.beyondtrust.com/products/privileged-remote-access
[bou]: https://www.HCP.com/products/boundary/features
[wal]: https://www.wallix.com/privileged-access-management/
[per]: https://www.perimeter81.com/zero-trust-application-access
[axi]: https://docs.axissecurity.com/docs/getting-started
[prx]: https://www.ssh.com/products/privileged-access-management-privx
[jmp]: https://github.com/jumpserver/jumpserver


[prxk8s]: https://privx.docs.ssh.com/docs/privx-on-kubernetes#architecture-diagram-for-privx-on-kubernetes
[bouflow]: https://developer.HCP.com/_next/image?url=https%3A%2F%2Fcontent.HCP.com%2Fapi%2Fassets%3Fproduct%3Dboundary%26version%3Drefs%252Fheads%252Fstable-website%26asset%3Dwebsite%252Fpublic%252Fimg%252Fboundary-network.png%26width%3D3740%26height%3D1820&w=3840&q=75&dpl=dpl_6zhDGUeikFi5Kzmcp23jcfifvrG4 