$version: "2"

namespace eu.europa.ec.snet.teleport

@error("client")
structure AlreadyExists {}

@error("client")
structure NotFound {}

@error("client")
structure UnknownInstance {}

@error("client")
structure InvalidVersion {}

@error("client")
structure InUse {}
