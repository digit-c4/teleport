$version: "2"

namespace eu.europa.ec.snet.teleport

resource TeleportTarget {
    identifiers: {
        id: Id
    }
    properties: {
        instance: Id,
        fqdn: Fqdn
    }
    create: RegisterTeleportTarget
    delete: UnregisterTeleportTarget
    list: ListTeleportTargets
}

structure TeleportTargetProperties for TeleportTarget {
    $instance
    $fqdn
}

@tags(["target"])
@documentation("Create new Teleport Target to instance")
@idempotent
@http(method: "POST", uri: "/api/plugins/teleport/target/")
operation RegisterTeleportTarget {
    input:= for TeleportTarget {
        @required
        $instance
        @required
        $fqdn
    }
    output:= for TeleportTarget {
        $instance
        $fqdn
    }
    errors: [UnknownInstance]
}

@tags(["target"])
@documentation("Remove Target with Id from Teleport Instance")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/teleport/target/{id}")
operation UnregisterTeleportTarget {
    input:= for TeleportTarget {
        @required
        @httpLabel
        $id
    }
    errors: [NotFound, UnknownInstance, InUse]
}

list ListTeleportTargetsList {
    member: TeleportTargetProperties
}

@tags(["target"])
@documentation("Show Targets")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/target/")
operation ListTeleportTargets {
    output:= {
        Targets: ListTeleportTargetsList
    }
}
