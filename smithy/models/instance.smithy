$version: "2"

namespace eu.europa.ec.snet.teleport


@tags(["instance"])
@documentation("Resource - Teleport Instance")
resource TeleportInstance {
    identifiers: {
        id: Id
    }
    properties: {
        fqdn: Fqdn
        exposure: InstanceExposure
	    slug: Slug
        state: InstanceState,
        operation: InstanceOperation
    }
    
    read: GetTeleportInstance
    create: CreateTeleportInstance
    delete: DeleteTeleportInstance
    list: ListTeleportInstances

    operations: [
        ModifyTeleportInstance
    ]
}

structure TeleportInstanceProperties for TeleportInstance {
    @required
    $id
    $slug
    $fqdn
    $exposure
}

@tags(["instance"])
@documentation("Show Instance based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/instance/{id}")
operation GetTeleportInstance {
    input := for TeleportInstance {
        @required
        @httpLabel
        $id
    }

    output := for TeleportInstance {
        $slug
        $fqdn
        $exposure
    }
}

@tags(["instance"])
@documentation("Create new Teleport Instance")
@idempotent
@http(method: "POST", uri: "/api/plugins/teleport/instance/")
operation CreateTeleportInstance {
    input:= for TeleportInstance {
        @required
        $slug
        @required
        $fqdn
        @required
        $exposure
    }
    output := for TeleportInstance {
        $slug
        $fqdn
        $exposure
    }
    errors: [AlreadyExists, InvalidVersion]
}

@tags(["instance"])
@documentation("Delete Teleport Instance with Id")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/teleport/instance/{id}")
operation DeleteTeleportInstance {
    input := for TeleportInstance {
        @required
        @httpLabel
        $id
    }
    errors: [NotFound]
}

list ListTeleportInstanceList for TeleportInstance {
    member: TeleportInstanceProperties
}

@tags(["instance"])
@documentation("Show Instance based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/instance/")
operation ListTeleportInstances {
    output:= for TeleportInstance {
        Instances: ListTeleportInstanceList
    }
}

@tags(["instance"])
@documentation("Delete Teleport Instance with Id")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/teleport/instance/{id}")
operation RotateMasterCredentialTeleportInstance {
    input := for TeleportInstance {
        @required
        @httpLabel
        $id
    }
    errors: [NotFound]
}

@tags(["instance"])
@documentation("Modify Teleport Instance with Id")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/instance/{id}")
operation ModifyTeleportInstance {
    input:= for TeleportInstance {
        @required
        @httpLabel
        $id
        state: InstanceState
        @required
        operation: InstanceOperation
    }
}
