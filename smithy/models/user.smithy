$version: "2"

namespace eu.europa.ec.snet.teleport

resource TeleportUser {
    identifiers: {
        id: Id
    }
    properties: {
        instance: Id,
        username: Username
        email: Email
        password: Password
    }
    read:ReadTeleportUser
    create: CreateTeleportUser
    update: UpdateTeleportUser
    list: ListTeleportUsers
    delete: DeleteTeleportUser
}

structure TeleportUserProperties for TeleportUser {
    @required
    $id
    $username
    $instance
    $email
    $password
}

@tags(["user"])
@documentation("Show User based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/user/{id}")
operation ReadTeleportUser {
    input := for  TeleportUser {
        @required
        @httpLabel
        $id
    }
    output := for TeleportUser {
        $username
        $instance
        $email
        $password
    }
    errors: [NotFound, UnknownInstance]
}

@tags(["user"])
@documentation("Create new Teleport User")
@idempotent
@http(method: "POST", uri: "/api/plugins/teleport/user/")
operation CreateTeleportUser {
    input:= for TeleportUser {
        @required
        $username
        @required
        $instance
        $email
        $password
    }
    output:= for TeleportUser {
        $username
        $instance
        $email
        $password
    }
    errors: [AlreadyExists, UnknownInstance]
}

@tags(["user"])
@documentation("Modify User Instance with Id")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/user/{id}")
operation UpdateTeleportUser {
    input:= for TeleportUser {
        @required
        @httpLabel
        $id
        $instance
        $username
        $email
        $password
    }
    output:= for TeleportUser {
        $username
        $instance
        $email
        $password
    }
    errors: [NotFound, UnknownInstance]
}

list ListTeleportUsersList for TeleportUser {
    member: TeleportUserProperties
}


@tags(["user"])
@documentation("Show users")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/user/")
operation ListTeleportUsers {
    output := {
        Users: ListTeleportUsersList
    }
    errors: [UnknownInstance]
}

@tags(["user"])
@documentation("Delete Teleport User with Id")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/teleport/user/{id}")
operation DeleteTeleportUser {
    input:= for TeleportUser {
        @required
        @httpLabel
        $id
    }
    errors: [NotFound, UnknownInstance]
}

