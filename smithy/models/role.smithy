$version: "2"

namespace eu.europa.ec.snet.teleport

@tags(["role"])
@documentation("Resource - Teleport Role")
resource TeleportRole {
    identifiers: {
        id: Id
    }
    properties: {
        users: TeleportRoleUserList
        accesses: TeleportRoleAccessList
        constraints: TeleportRoleConstraintList
        slug: Slug
    }

    create: CreateTeleportRole
    delete: DeleteTeleportRole
    list: ListTeleportRoles
    operations: [
        AddUsersTeleportRole,
        DeleteUserTeleportRole,
        AddAccessTeleportRole,
        DeleteAccessTeleportRole
    ]
}

structure TeleportRoleAccess {
    target: Fqdn
    protocol: Protocols
    port: Port
}

list TeleportRoleAccessList {
    member: TeleportRoleAccess
}

list TeleportRoleUserList {
    member: Username
}

structure TeleportAccessConstraints {
    prefix: Ipv4Prefix
    tag: Tag
}

list TeleportRoleConstraintList {
    member: TeleportAccessConstraints
}

@tags(["role"])
@documentation("Create new Teleport Role")
@idempotent
@http(method: "POST", uri: "/api/plugins/teleport/role/")
operation CreateTeleportRole {
    input:= for TeleportRole {
        @required
        $slug
        @required
        $constraints
        $users
        $accesses
    }
    errors: [AlreadyExists,UnknownInstance]
}

@tags(["role"])
@documentation("Delete Teleport Role")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/teleport/role/{id}")
operation DeleteTeleportRole {
    input:= for TeleportRole {
        @required
        @httpLabel
        $id
    }
    errors: [NotFound, UnknownInstance, InUse]
}

structure TeleportRoleProperties for TeleportRole {
    @required
    $slug
    $users
    $accesses
    $constraints
}

list ListTeleportRoleList {
    member: TeleportRoleProperties
}

@tags(["role"])
@documentation("Show Roles based on slug")
@readonly
@http(method: "GET", uri: "/api/plugins/teleport/role/{slug}")
operation ListTeleportRoles {
    input:= for TeleportRole {
        @required
        @httpLabel
        $slug
    }
    output:= {
        roles: ListTeleportRoleList
    }
    errors: [UnknownInstance]
}

@tags(["role"])
@documentation("Add user to role")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/role/{id}/add-user")
operation AddUsersTeleportRole {
    input:= for TeleportRole {
        @required
        @httpLabel
        $id
        @required
        $users
    }
    errors: [NotFound, UnknownInstance]
}

@tags(["role"])
@documentation("Add user to role")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/role/{id}/remove-user")
operation DeleteUserTeleportRole {
    input:= for TeleportRole {
        @required
        @httpLabel
        $id
        @required
        $users
    }
    errors: [NotFound, UnknownInstance]
}

@tags(["role"])
@documentation("Add access to role")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/role/{id}/add-access")
operation AddAccessTeleportRole {
    input:= for TeleportRole {
        @required
        @httpLabel
        $id
        @required
        $slug
    }
    errors: [NotFound, UnknownInstance]
}

@tags(["role"])
@documentation("Delete access to role")
@idempotent
@http(method: "PUT", uri: "/api/plugins/teleport/role/{id}/remove-access")
operation DeleteAccessTeleportRole {
    input:= for TeleportRole {
        @required
        @httpLabel
        $id
        @required
        $slug
    }
    errors: [NotFound, UnknownInstance]
}


