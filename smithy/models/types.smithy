$version: "2"

namespace eu.europa.ec.snet.teleport

enum InstanceExposure {
    INTERNAL
    EXTERNAL
}

enum InstanceState {
    CREATING
    CREATED
    STARTED
    STOPPED
    RESTARTING
}

enum InstanceOperation {
    STOP
    RESTART
    BACKUP
    RESTORE
    ROTATE
    UPGRADE
    NONE
}

enum Protocols {
    SSH
}

@pattern("^[A-Za-z0-9-_]+$")
string Id

@pattern("^[A-Za-z0-9-_]+$")
string Slug

@pattern("^((?!-)[\\w-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$")
string Fqdn

string Version

@pattern("^[\\w]{4,20}$")
string Username

@pattern("^[\\w\\-\\.]+@([\\w-]+\\.)+[\\w-]{2,}$")
string Email

string Password

@range(min: 0, max: 4095)
integer Port

@pattern("^(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])){3}/(3[0-2]|[1-2]?[0-9])$")
string Ipv4Prefix

@pattern("^[\\w]{4,20}$")
string Tag

timestamp InstanceBackupId
