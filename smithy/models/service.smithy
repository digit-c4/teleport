$version: "2"

namespace eu.europa.ec.snet.teleport

use aws.protocols#restJson1

@restJson1
service Teleport {
     version: "0.0.2"
     resources: [TeleportInstance, TeleportUser, TeleportTarget, TeleportRole]
}
