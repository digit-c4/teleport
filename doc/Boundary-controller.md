

# Links to doc

[Boundary flows](https://developer.hashicorp.com/_next/image?url=https%3A%2F%2Fcontent.hashicorp.com%2Fapi%2Fassets%3Fproduct%3Dboundary%26version%3Drefs%252Fheads%252Fstable-website%26asset%3Dwebsite%252Fpublic%252Fimg%252Fboundary-network.png%26width%3D3740%26height%3D1820&w=3840&q=75&dpl=dpl_6zhDGUeikFi5Kzmcp23jcfifvrG4)


[Sizing](https://developer.hashicorp.com/boundary/docs/install-boundary/architecture/system-requirements)

[Pricing](https://www.hashicorp.com/products/boundary/pricing)

[MFA](https://github.com/hashicorp/boundary/issues/867)



# TODO
- Understand flows
- Understand KMS usage
- Account vs User


# Boundary client

## Client authentication

### Password authentication
```bash
boundary authenticate password  -addr=https://boundary.xxx:443 -keyring-type=none

Please enter the login name (it will be hidden):
Please enter the password (it will be hidden):

Authentication information:
  Account ID:      acctpw_0987654321
  Auth Method ID:  ampw_1234567890
  Expiration Time: Mon, 29 Jul 2024 14:49:36 CEST
  User ID:         u_0987654321

Storing the token in a keyring was disabled. The token is:
at_qNLffQ0aob_s125uB3Q9hXMo3AAXxZEFMzKr8RXEJtmnp9eGcLyxReQnEomy2pweQ5cMcR8e
Please be sure to store it safely!
```

## Client connection

```bash
./boundary connect ssh ssh.boundary.dev  -adddr=https://boundary.xxx443 -token env://BOUNDARY_TOKEN -username root
olivier@x570:/data/Comp/Hashicorp-Boundary> ./boundary connect ssh ssh.boundary.dev  -addr=https://boundary.xxxx:443 -token env://BOUNDARY_TOKEN -username root
The authenticity of host 'ttcp_r2cdxgobyn ([127.0.0.1]:34371)' can't be established.
ED25519 key fingerprint is SHA256:0MCcPxYEHU7piwwBOgVYptR/50YU4qgzG6QLrDUo5zo.
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:396: 155.56.193.2
    ~/.ssh/known_hosts:450: ttcp_lxhg35l37y
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'ttcp_r2cdxgobyn' (ED25519) to the list of known hosts.
Last login: Sun Jul 14 22:21:56 CEST 2024 from 127.0.0.1 on ssh
```


### Proxy support
```
The desktop app supports using --proxy-server=[proxy URL] to connect to the API server per the Electron documentation 9. Note that you have to use the = form of the flag; --proxy-server [proxy URL] with a space instead of = did not work when I was testing.

The Boundary CLI appears to support the lowercase forms of the usual *_proxy variables for connecting to the API server
```
TODO:
- test
- check basic auth support  ( [ntml auth not supported](https://github.com/hashicorp/boundary/issues/2237) )


## Status 
```bash
> ./boundary daemon status

Status:
  Domain Socket:       .boundary/socket/daemon.sock
  Log Location:        .boundary/cache.log
  Uptime:              21m13s
  User Count:          1
  Version:             Boundary v0.16.2 (e2f22f766430c522684cb75d229554c610e13eef)

  User:
    Address:                        https://boundary.xxx:443
    AuthToken Count:                1
    Id:                             u_0987654321
    Search Support:                 Supported
    Since Search Support Check:     2m29s
    AuthToken:
      Id:                      at_qNLffQ0aob
    Resolvable-Alias:
      Count:                     3
      Since Initial Fetch:       21m13s
      Since Last Refresh:        2m29s
    Target:
      Count:                     5
      Since Initial Fetch:       21m13s
      Since Last Refresh:        2m29s
    Session:
      Count:                   0
```




# Controller Ops

## Create user
**!!!!!! user != account   !!!!!!**
```bash
./boundary users create  -token="env://BOUNDARY_TOKEN" -addr=https://xxx.eu:443
Error from controller when performing create on user

Error information:
  Kind:                PermissionDenied
  Message:             Forbidden.
  Status:              403
  context:             Error from controller when performing create on user
```



# Installation

## Network requirement
    Clients must have access to the Controller's api port (default 9200)
    Workers must have access to the Controller's cluster port (default 9201)
    Clients must have access to the Worker's port (default 9202)

    Workers must have a route and port access to the hosts defined within the system in order to provide connectivity


## Database
 PostgreSQL + citext and pgcrypto modules

```sql
CREATE ROLE boundary WITH LOGIN PASSWORD 'PASSWORD';
CREATE DATABASE boundary OWNER boundary;
GRANT ALL PRIVILEGES ON DATABASE boundary TO boundary;
ALTER USER boundary PASSWORD 'PASSWORD';
```

## Controller conf
```bash
# Disable memory lock: https://www.man7.org/linux/man-pages/man2/mlock.2.html
disable_mlock = true
hcp_boundary_cluster_id = "ECLAB"


controller {
  name = "lab_controller"
  description = "HCP Controller"
  database {
    url = "postgresql://User:Passs@DBServer.local:5432/boundary?sslmode=disable"
    max_open_connections = 5
  }
  public_cluster_addr = "boundary.local:9201"
  auth_token_time_to_live  = "7d"

  # Rate limiting examples to conserve controller resources
  # total limit for all resources and actions
  api_rate_limit {
    resources = ["*"]
    actions   = ["*"]
    per     = "total"
    limit   = 500
    period  = "1s"
  }

  # Limit for ip addresses to all resources+actions to prevent a malicious
  # host that is fabricating tokens, or spamming unauthed endpoints
  api_rate_limit {
    resources = ["*"]
    actions   = ["*"]
    per     = "ip-address"
    limit   = 100
    period  = "1s"
  }

  # Limit of all authed requests, to prevent one user consuming all of the total limit
  api_rate_limit {
    resources = ["*"]
    actions   = ["*"]
    per     = "auth-token"
    limit   = 100
    period  = "1s"
  }
}


# API listener configuration block
listener "tcp" {
  # Should be the address of the NIC that the controller server will be reached on
  address = "192.168.1.1"
  # The purpose of this listener block
  purpose = "api"

  tls_disable = false

  tls_cert_file = "boundary-priv.pem"
  tls_key_file  = "boundary-priv.key"


  # Uncomment to enable CORS for the Admin UI. Be sure to set the allowed origin(s)
  # to appropriate values.
  #cors_enabled = true
  #cors_allowed_origins = ["https://yourcorp.yourdomain.com", "serve://boundary"]
}

# Data-plane listener configuration block (used for worker coordination)
listener "tcp" {
  # Should be the IP of the NIC that the worker will connect on
  address = "192.168.1.1"
  # The purpose of this listener
  purpose = "cluster"
}

listener "tcp" {
  # Should be the address of the NIC where your external systems'
  # (eg: Load-Balancer) will connect on.
  address = "192.168.1.1"
  # The purpose of this listener block
  purpose = "ops"

  tls_disable = false

  tls_cert_file = "boundary-priv.pem"
  tls_key_file  = "boundary-priv.key"
}


# key <- openssl  rand -base64  32
kms "aead" {
    purpose = "root"
    aead_type = "aes-gcm"
    key = "egvdhBKLNMZZaE3ywEN+AkOvCfjh7+p0GtRBQFCuPbbdOsA="
    key_id = "global_root"
}


# key <- openssl  rand -base64  32
kms "aead" {
    purpose = "worker-auth"
    aead_type = "aes-gcm"
    key = "egvdhBKLNMZZaE3EGLQldGY4+iE9AkOvCfjh7+p0GtRBQ="
    key_id = "global_worker-auth"
}


# Events (logging) configuration. This
# configures logging for ALL events to both
# stderr and a file at /var/log/boundary/controller.log
events {
  audit_enabled       = true
  sysevents_enabled   = true
  observations_enable = true
  sink "stderr" {
    name = "all-events"
    description = "All events sent to stderr"
    event_types = ["*"]
    format = "cloudevents-json"
  }
  sink {
    name = "file-sink"
    description = "All events sent to a file"
    event_types = ["*"]
    format = "cloudevents-json"
    file {
      path = "/var/log/boundary"
      file_name = "controller.log"
    }
    audit_config {
      audit_filter_overrides {
        sensitive = "redact"
        secret    = "redact"
      }
    }
  }
```



## DB init
```bash
/boundary database init  -config=controller.hcl   

Migrations successfully run.
Global-scope KMS keys successfully created.

Initial login role information:
  Name:      Login Grants
  Role ID:   r_2SojC9sXNT

Initial authenticated user role information:
  Name:      Authenticated User Grants
  Role ID:   r_mVZrp1Wu5r

Initial auth information:
  Auth Method ID:     ampw_PYO9fb7ju1
  Auth Method Name:   Generated global scope initial password auth method
  Login Name:         admin
  Password:           ?????????
  Scope ID:           global
  User ID:            u_H7TCHOFfNb
  User Name:          admin

Initial org scope information:
  Name:       Generated org scope
  Scope ID:   o_VWav897bp5
  Type:       org

Initial project scope information:
  Name:       Generated project scope
  Scope ID:   p_T1SI2bzeJD
  Type:       project

Initial host resources information:
  Host Catalog ID:     hcst_qMMaoAFRwP
  Host Catalog Name:   Generated host catalog
  Host ID:             hst_JBUZDMNVmI
  Host Name:           Generated host
  Host Set ID:         hsst_O2ka9bm5pe
  Host Set Name:       Generated host set
  Scope ID:            p_T1SI2bzeJD
  Type:                static

Initial target information:
  Default Port:               22
  Name:                       Generated target with a direct address
  Scope ID:                   p_T1SI2bzeJD
  Session Connection Limit:   -1
  Session Max Seconds:        28800
  Target ID:                  ttcp_IiqlcfzpdB
  Type:                       tcp

Initial target information:
  Default Port:               22
  Name:                       Generated target using host sources
  Scope ID:                   p_T1SI2bzeJD
  Session Connection Limit:   -1
  Session Max Seconds:        28800
  Target ID:                  ttcp_ZOzk6sJcTx
  Type:                       tcp
```

## Minimal DB 
```bash
boundary database init \
   -skip-auth-method-creation \
   -skip-host-resources-creation \
   -skip-scopes-creation \
   -skip-target-creation \
   -config /etc/boundary.d/controller.hcl
```

```bash
Migrations successfully run.
Global-scope KMS keys successfully created.

Initial login role information:
  Name:      Login Grants
  Role ID:   r_m88uRzVu9q

Initial authenticated user role information:
  Name:      Authenticated User Grants
  Role ID:   r_wrk7FrfdKq
```

## Use of recovery workflow

Until a valid org admin is created with proper grants, the "recovery" workflow will use the recovery KMS to "authenticate".

```text
kms "aead" {
        purpose   = "recovery"
        aead_type = "aes-gcm"
        key       = "8fZBjCUfN0TzjEGLQldGY4+iE9AkOvCfjh7+p0GtRBQ="
        key_id    = "global_recovery"
}
```

To use the recovery workflow on the CLI, you must pass the -recovery-config <path_to_kms_recovery_config> flag
```bash
boundary CMD SUBCMD -recovery-config recovery.hcl
```




## Create new top level org

```bash
boundary scopes create -name 'LAB' -scope-id 'global' \
  -recovery-config recovery.hcl \
  -skip-admin-role-creation \
  -skip-default-role-creation \


Scope information:
  Created Time:        Wed, 31 Jul 2024 17:24:01 CEST
  ID:                  o_V6UdqSEfVz
  Name:                LAB
  Updated Time:        Wed, 31 Jul 2024 17:24:01 CEST
  Version:             1

  Scope (parent):
    ID:                global
    Name:              global
    Type:              global

  Authorized Actions:
    read
    update
    delete
    attach-storage-policy
    detach-storage-policy
    no-op

  Authorized Actions on Scope's Collections:
    auth-methods:
      create
      list
    auth-tokens:
      list
    groups:
      create
      list
    policies:
      create
      list
    roles:
      create
      list
    scopes:
      create
      list
      list-keys
      rotate-keys
      list-key-version-destruction-jobs
      destroy-key-version
    session-recordings:
      list
    storage-buckets:
      create
      list
    users:
      create
      list
```

## Create a project

Get the ID of the ORG previously generated (`o_V6UdqSEfVz`) to create a project under that ORG

```bash
boundary scopes create -name "bubble" -scope-id o_V6UdqSEfVz \
  -recovery-config recovery.hcl \
  -skip-admin-role-creation \
  -skip-default-role-creation 


Scope information:
  Created Time:        Wed, 31 Jul 2024 17:32:35 CEST
  ID:                  p_1nixz57LLV
  Name:                bubble
  Updated Time:        Wed, 31 Jul 2024 17:32:35 CEST
  Version:             1

  Scope (parent):
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    delete
    no-op
    read
    update

  Authorized Actions on Scope's Collections:
    credential-stores:
      create
      list
    groups:
      create
      list
    host-catalogs:
      list
      create
    roles:
      list
      create
    scopes:
      list-keys
      rotate-keys
      list-key-version-destruction-jobs
      destroy-key-version
    sessions:
      list
    targets:
      create
      list
```


## Create a password authentication method
Set min password length and account name

```bash
boundary auth-methods create password \
  -name "PASSWORD" \
  -description "Password auth method" \
  -scope-id o_V6UdqSEfVz \ 
  -recovery-config recovery.hcl  \
  -min-login-name-length=6 \
  -min-password-length=16



Auth Method information:
  Created Time:                Wed, 31 Jul 2024 17:39:16 CEST
  Description:                 Password auth method
  ID:                          ampw_CLVNuA2EAV
  Name:                        PASSWORD
  Type:                        password
  Updated Time:                Wed, 31 Jul 2024 17:39:16 CEST
  Version:                     1

  Scope:
    ID:                        o_V6UdqSEfVz
    Name:                      LAB
    Parent Scope ID:           global
    Type:                      org

  Authorized Actions:
    update
    delete
    authenticate
    no-op
    read

  Authorized Actions on Auth Method's Collections:
    accounts:
      create
      list
    managed-groups:
      create
      list

  Attributes:
    Minimum Login Name Length: 3
    Minimum Password Length:   8
```
### Define a default authentication method for a scope

```bash
boundary scopes update -id=o_V6UdqSEfVz -primary-auth-method-id=ampw_CLVNuA2EAV

Scope information:
  Created Time:             Wed, 31 Jul 2024 17:24:01 CEST
  ID:                       o_V6UdqSEfVz
  Name:                     LAB
  Primary Auth Method ID:   ampw_CLVNuA2EAV
  Updated Time:             Fri, 02 Aug 2024 17:20:06 CEST
  Version:                  2

  Scope (parent):
    ID:                     global
    Name:                   global
    Type:                   global

  Authorized Actions:
    update
    delete
    attach-storage-policy
    detach-storage-policy
    no-op
    read

  Authorized Actions on Scope's Collections:
    auth-methods:
 
```

## Create a user 

```bash
boundary users create \
  -scope-id o_V6UdqSEfVz \
  -name "admin" \
  -description "Last resort admin" \
  -recovery-config recovery.hcl 


  User information:
  Created Time:        Wed, 31 Jul 2024 17:49:52 CEST
  Description:         Last resort admin
  ID:                  u_xCoymIoVrZ
  Name:                admin
  Updated Time:        Wed, 31 Jul 2024 17:49:52 CEST
  Version:             1

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    update
    delete
    add-accounts
    set-accounts
    remove-accounts
    list-resolvable-aliases
    no-op
    read

```

# Create an account

Create an account that will use the password authenticaiton method (identified by its id)

```bash
boundary accounts create password  \
  -login-name "admin" \
  -auth-method-id ampw_CLVNuA2EAV \
  -recovery-config recovery.hcl


Please enter the password (it will be hidden): ****************
Please enter it one more time for confirmation: ****************

Account information:
  Auth Method ID:      ampw_CLVNuA2EAV
  Created Time:        Wed, 31 Jul 2024 17:54:28 CEST
  ID:                  acctpw_3fLwXDYk2i
  Type:                password
  Updated Time:        Wed, 31 Jul 2024 17:54:28 CEST
  Version:             1

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    no-op
    read
    update
    delete
    set-password
    change-password

  Attributes:
    Login Name:        admin
```

##  Add password based account to a user
The user will have an assoiciated account to authenticate with password method.

```bash
boundary users add-accounts \
  -id  u_xCoymIoVrZ \
  -account acctpw_3fLwXDYk2i \
  -recovery-config recovery.hcl 

User information:
  Created Time:        Wed, 31 Jul 2024 17:49:52 CEST
  Description:         Last resort admin
  ID:                  u_xCoymIoVrZ
  Name:                admin
  Updated Time:        Wed, 31 Jul 2024 17:58:47 CEST
  Version:             2

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    list-resolvable-aliases
    no-op
    read
    update
    delete
    add-accounts
    set-accounts
    remove-accounts

  Accounts:
    ID:                acctpw_3fLwXDYk2i
    Scope ID:          o_V6UdqSEfVz
```

##  Create an admin role in scope LAB

```bash
boundary roles create -name "Role-org-admin" \
  -scope-id 'o_V6UdqSEfVz' \
  -recovery-config recovery.hcl


Role information:
  Created Time:        Thu, 01 Aug 2024 12:13:58 CEST
  Grant Scope ID:      this
  ID:                  r_PqEIGJS6B7
  Name:                Role-org-admin
  Updated Time:        Thu, 01 Aug 2024 12:13:58 CEST
  Version:             2

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    set-grants
    set-grant-scopes
    read
    delete
    set-principals
    add-grants
    remove-grants
    add-grant-scopes
    remove-grant-scopes
    no-op
    update
    add-principals
    remove-principals

  Grant Scope IDs:
    ID:             this

```
##  Grant all actions to the created role
```bash
boundary roles add-grants -id  r_PqEIGJS6B7 \
  -recovery-config recovery.hcl \
  -grant 'ids=*;type=*;actions=*' \

Role information:
  Created Time:        Thu, 01 Aug 2024 12:13:58 CEST
  Grant Scope ID:      this
  ID:                  r_PqEIGJS6B7
  Name:                Role-org-admin
  Updated Time:        Thu, 01 Aug 2024 14:28:24 CEST
  Version:             3

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    add-grants
    set-grants
    set-grant-scopes
    read
    delete
    set-principals
    remove-principals
    remove-grants
    add-grant-scopes
    remove-grant-scopes
    no-op
    update
    add-principals

  Canonical Grants:
    ids=*;type=*;actions=*

  Grant Scope IDs:
    ID:             this

```

## Assign role ORG_admin to user admin
```bash
boundary roles add-principals -id r_OUuIO0OyTe \
  -recovery-config recovery.hcl \
  -principal u_xCoymIoVrZ \
  -tls-insecure


Role information:
  Created Time:        Thu, 01 Aug 2024 10:54:17 CEST
  Grant Scope ID:      o_V6UdqSEfVz
  ID:                  r_OUuIO0OyTe
  Name:                Role-org-admin
  Updated Time:        Thu, 01 Aug 2024 11:21:10 CEST
  Version:             4

  Scope:
    ID:                global
    Name:              global
    Type:              global

  Authorized Actions:
    add-grants
    set-grants
    add-grant-scopes
    read
    update
    add-principals
    remove-principals
    remove-grants
    set-grant-scopes
    remove-grant-scopes
    no-op
    delete
    set-principals

  Principals:
    ID:             u_xCoymIoVrZ
      Type:         user
      Scope ID:     o_V6UdqSEfVz

  Canonical Grants:
    ids=*;type=*;actions=*

  Grant Scope IDs:
    ID:             o_V6UdqSEfVz

```

# Create a group
```bash
boundary groups create -name "g_sys" -description="SYS members"

Group information:
  Created Time:        Thu, 01 Aug 2024 17:56:39 CEST
  Description:         SYS members
  ID:                  g_esZLZWg63k
  Name:                g_sys
  Updated Time:        Thu, 01 Aug 2024 17:56:39 CEST
  Version:             1

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    update
    delete
    add-members
    set-members
    remove-members
    no-op
    read
```


## Add member to a group

Add snet to group g_sys

```bash
 boundary groups add-members -id  g_esZLZWg63k  -member  u_KWDGMyMjAG

Group information:
  Created Time:        Thu, 01 Aug 2024 17:56:39 CEST
  Description:         SYS members
  ID:                  g_esZLZWg63k
  Name:                g_sys
  Updated Time:        Thu, 01 Aug 2024 17:58:35 CEST
  Version:             2

  Scope:
    ID:                o_V6UdqSEfVz
    Name:              LAB
    Parent Scope ID:   global
    Type:              org

  Authorized Actions:
    set-members
    remove-members
    no-op
    read
    update
    delete
    add-members

  Members:
    ID:                u_KWDGMyMjAG
    Scope ID:          o_V6UdqSEfVz
```


## Create a host catalog for the SYS project
A host catalog is a resource that contains hosts and host sets. A host catalog can **only** be defined within a project.

Create a host catalog for the SYS project

```bash
 boundary host-catalogs  create static -scope-id=p_1nixz57LLV -name "SYS_targets" -description="SYS squad hosts"

Host Catalog information:
  Created Time:        Thu, 01 Aug 2024 18:09:46 CEST
  Description:         SYS squad hosts
  ID:                  hcst_YQ09icuY70
  Name:                SYS_targets
  Type:                static
  Updated Time:        Thu, 01 Aug 2024 18:09:46 CEST
  Version:             1

  Scope:
    ID:                p_1nixz57LLV
    Name:              SYS_squad
    Parent Scope ID:   o_V6UdqSEfVz
    Type:              project

  Authorized Actions:
    no-op
    read
    update
    delete

  Authorized Actions on Host Catalog's Collections:
    host-sets:
      create
      list
    hosts:
      create
      list
```


 boundary host-catalogs create static -scope-id=p_1nixz57LLV -name "SYS-host-catalog" -description="SYS squad host catalog"

Host Catalog information:
  Created Time:        Fri, 02 Aug 2024 17:36:53 CEST
  Description:         SYS squad host catalog
  ID:                  hcst_ZIWm1LtfqB
  Name:                SYS-host-catalog
  Type:                static
  Updated Time:        Fri, 02 Aug 2024 17:36:53 CEST
  Version:             1

  Scope:
    ID:                p_1nixz57LLV
    Name:              SYS_squad
    Parent Scope ID:   o_V6UdqSEfVz
    Type:              project

  Authorized Actions:
    no-op
    read
    update
    delete

  Authorized Actions on Host Catalog's Collections:
    host-sets:
      create
      list
    hosts:
      create
      list


## Add host to a host catalog
The same host created in two catalog will e create as two disctinct objects with their own unique ID.

```bash
 boundary hosts create static -name "test-server" -description="TP OVH sbg" -address="192.168.8.1" -host-catalog-id=hcst_ZIWm1LtfqB

Host information:
  Created Time:        Fri, 02 Aug 2024 17:41:39 CEST
  Description:         TP OVH sbg
  Host Catalog ID:     hcst_ZIWm1LtfqB
  ID:                  hst_E29ektcNJM
  Name:                sbg
  Type:                static
  Updated Time:        Fri, 02 Aug 2024 17:41:39 CEST
  Version:             1

  Scope:
    ID:                p_1nixz57LLV
    Name:              SYS_squad
    Parent Scope ID:   o_V6UdqSEfVz
    Type:              project

  Authorized Actions:
    no-op
    read
    update
    delete

  Attributes:
    address:           192.168.8.1
```


##  Create target
Targets are Boundary resources that contain one or more host sets. A target allows Boundary users to define an endpoint with a default port and a protocol to establish a session. Unless specified with a -host-id flag, Boundary will choose one Host in the host set to connect to at random.

```bash
boundary targets create tcp -name "Target_ssh" -description="Target SSH" -default-port=22 -scope-id=p_yzBGyI5NeQ  -session-connection-limit="-1"

Target information:
  Created Time:               Mon, 05 Aug 2024 11:13:01 CEST
  Description:                Target SSH
  ID:                         ttcp_JQfbNUtVll
  Name:                       Target_ssh
  Session Connection Limit:   -1
  Session Max Seconds:        28800
  Type:                       tcp
  Updated Time:               Mon, 05 Aug 2024 11:13:01 CEST
  Version:                    1

  Scope:
    ID:                       p_yzBGyI5NeQ
    Name:                     TCDC_squad
    Parent Scope ID:          o_V6UdqSEfVz
    Type:                     project

  Authorized Actions:
    set-host-sources
    add-host-sources
    read
    update
    delete
    remove-host-sources
    add-credential-sources
    set-credential-sources
    remove-credential-sources
    no-op
    authorize-session

  Attributes:
    Default Port:             22
```

## Host sets
A target works off of host sets. Therefore, even if there is only one host, you still create a host set containing one host.

### Create a host set
```bash
boundary host-sets create static -name "test server" -description "test server" -host-catalog-id=hcst_qsbKcV9LJP


Host Set information:
  Created Time:        Mon, 05 Aug 2024 11:27:17 CEST
  Description:         test server
  Host Catalog ID:     hcst_qsbKcV9LJP
  ID:                  hsst_jeKnqmyGuY
  Name:                test server
  Type:                static
  Updated Time:        Mon, 05 Aug 2024 11:27:17 CEST
  Version:             1

  Scope:
    ID:                p_1nixz57LLV
    Name:              SYS_squad
    Parent Scope ID:   o_V6UdqSEfVz
    Type:              project

  Authorized Actions:
    delete
    add-hosts
    set-hosts
    remove-hosts
    no-op
    read
    update
```

### Add host a host set
```bash
 boundary host-sets add-hosts -id=hsst_jeKnqmyGuY -host=hst_UkGh9HEIVJ

Host Set information:
  Created Time:        Mon, 05 Aug 2024 11:27:17 CEST
  Description:         test server
  Host Catalog ID:     hcst_qsbKcV9LJP
  ID:                  hsst_jeKnqmyGuY
  Name:                test server
  Type:                static
  Updated Time:        Mon, 05 Aug 2024 11:35:24 CEST
  Version:             2

  Scope:
    ID:                p_1nixz57LLV
    Name:              SYS_squad
    Parent Scope ID:   o_V6UdqSEfVz
    Type:              project

  Authorized Actions:
    remove-hosts
    no-op
    read
    update
    delete
    add-hosts
    set-hosts

  Host IDs:
    hst_UkGh9HEIVJ
```

catalalog -> host 
hosts-sets <- host
target <- hosts-sets  +port 

https://developer.hashicorp.com/boundary/tutorials/hcp-administration/hcp-manage-targets


## Create targets 
Create a TCP target on port 22


```bash
boundary targets create tcp -name "Target_ssh" -description="Target SSH" -default-port=22 -scope-id=p_1nixz57LLV  -session-connection-limit="-1"

Target information:
  Created Time:               Mon, 05 Aug 2024 11:39:55 CEST
  Description:                Target SSH
  ID:                         ttcp_XCERES7sFm
  Name:                       Target_ssh
  Session Connection Limit:   -1
  Session Max Seconds:        28800
  Type:                       tcp
  Updated Time:               Mon, 05 Aug 2024 11:39:55 CEST
  Version:                    1

  Scope:
    ID:                       p_1nixz57LLV
    Name:                     SYS_squad
    Parent Scope ID:          o_V6UdqSEfVz
    Type:                     project

  Authorized Actions:
    set-credential-sources
    remove-credential-sources
    no-op
    read
    update
    delete
    remove-host-sources
    add-credential-sources
    authorize-session
    add-host-sources
    set-host-sources

  Attributes:
    Default Port:             22
```


### Add host-set to target


```bash
 boundary targets add-host-sources -id=ttcp_XCERES7sFm -host-source=hsst_jeKnqmyGuY

Target information:
  Created Time:               Mon, 05 Aug 2024 11:39:55 CEST
  Description:                Target SSH
  ID:                         ttcp_XCERES7sFm
  Name:                       Target_ssh
  Session Connection Limit:   -1
  Session Max Seconds:        28800
  Type:                       tcp
  Updated Time:               Mon, 05 Aug 2024 11:44:10 CEST
  Version:                    2

  Scope:
    ID:                       p_1nixz57LLV
    Name:                     SYS_squad
    Parent Scope ID:          o_V6UdqSEfVz
    Type:                     project

  Authorized Actions:
    add-host-sources
    set-host-sources
    remove-host-sources
    add-credential-sources
    set-credential-sources
    remove-credential-sources
    no-op
    read
    update
    delete
    authorize-session

  Host Sources:
    Host Catalog ID:          hcst_qsbKcV9LJP
    ID:                       hsst_jeKnqmyGuY

  Attributes:
    Default Port:             22
```

# Logging & Monitoring

## TODO
-  client_ip & RPS : XFF ?

```json
{
  "id": "M8JT1XoPfV",
  "source": "https://hashicorp.com/boundary/x570/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_lliQiCL6SL",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-07-25T09:28:43.504151978+02:00",
    "request_info": {
      "id": "gtraceid_PlyP40C5NsnH20sYyW9U",
      "method": "GET",
      "path": "/v1/auth-methods?scope_id=global",
      "public_id": "at_EHkWY1bG43",
      "client_ip": "192.168.3.1"
    }
  }
```



# Register a new worker



Workers connect to upstreams (Controllers, or other Workers) via TLS connections. In order to make these connections, Workers have to be authenticated to the cluster. How this authentication and session establishment takes place can happen in one of two ways: by a built-in X.509-based PKI system, or by leveraging a KMS system to provide secure transport of authentication information.

The KMS-based mechanism provides zero-touch registration of Workers to a cluster, while the PKI mechanism provides positive access control at registration time. Currently, HCP Boundary deployments only support PKI authentication for self-managed Workers.


```bash
 ../boundary server -config="worker.hcl"
==> Boundary server configuration:

                               Cgo: disabled
                        Listener 1: tcp (addr: "0.0.0.0:9202", max_request_duration: "1m30s", purpose: "proxy")
                         Log Level: info
                             Mlock: supported: true, enabled: false
                           Version: Boundary v0.16.2
                       Version Sha: e2f22f766430c522684cb75d229554c610e13eef
        Worker Auth Current Key Id: choosy-carrousel-myself-ambiguous-chaos-brussels-trifocals-amicably
  Worker Auth Registration Request: GzusqckarbczHoLGQ4UA25uSS63nHf4iryiqCmyphGZLx45YZP8ibA7uBe9Bs2xZbuPfTFFinxsFJiqmCu7HFpwQ9EY7MiAcC6Gp6wwt3XUUj22Sy4h4gBjToYUpcN2MiHrWCUWNMZf7XLgFKfK5xMZBWasEBQkoCQB7EdtRVULoJzeXezu2g4qzo2uEecPyWQVwHjbLNQuveyQBfpLhFz4QhWvdp83diSkYEVeQVzgtaiDYVH3FTjRXk9iQgQHsi9RWB43RTaKC5E23fJ46kLctDXSE19QxfMyx1DJKwW
          Worker Auth Storage Path: /data/Comp/Hashicorp-Boundary/worker/worker1
          Worker Public Proxy Addr: 192.168.3.159:9202

==> Boundary server started! Log data will stream in below:
```

Paste the "Worker Auth Registration Request" to te controller


Error from controller when performing create on worker-led-type worker

Error information:
  Kind:                InvalidArgument
  Message:             Error in provided request.
  Status:              400
  context:             Error from controller when performing create on worker-led-type worker

  Field-specific Errors:
    Name:              -scope-id


### worker to controller with mulitple IPs
Worker will register to the controller on API service 9200 but then the controller must listen on port 9201 to accept  worker "data" connection.

### Worker-to-upstream TLS

Workers connect to upstreams (Controllers, or other Workers) via TLS connections. In order to make these connections, Workers have to be authenticated to the cluster. How this authentication and session establishment takes place can happen in one of two ways: by a built-in X.509-based PKI system, or by leveraging a KMS system to provide secure transport of authentication information.

The KMS-based mechanism provides zero-touch registration of Workers to a cluster, while the PKI mechanism provides positive access control at registration time. Currently, HCP Boundary deployments only support PKI authentication for self-managed Workers.


## PKI-based worker authentication
https://developer.hashicorp.com/boundary/docs/concepts/security/connections-tls#worker-to-upstream-tls
### Worker-led PKI-based registration
### Controller-led PKI-based registration

### 
admin of the scope "LAB" cannot 

```
boundary workers list -scope-id=global -token env://BOUNDARY_TOKEN
Error from controller when performing list on workers

Error information:
  Kind:                PermissionDenied
  Message:             Forbidden.
  Status:              403
  context:             Error from controller when performing list on workers
```

### Create a superadmin in the global scope

```bash
boundary users create -scope-id=global   -name "superadmin"   -description "Gobal Superadmin"
boundary auth-methods create password   -name "Global_passwd-auth" -description "Password auth method for Global"
boundary scopes update -primary-auth-method-id=ampw_eWzqdWXIOV -id=global
boundary accounts create password  -login-name="superadmin"  -auth-method-id=ampw_eWzqdWXIOV
boundary users add-accounts -id=u_gUJ9fSfpg8 -account=acctpw_va1kY1STJN -scope-id=global
boundary roles create -name "Role-Global-admin"  -scope-id=global
boundary roles add-grants -id=r_crB2i3moB7 -grant 'ids=*;type=*;actions=*'  -scope-id=global
boundary roles add-principals -id=r_crB2i3moB7  -principal=u_gUJ9fSfpg8 -scope-id=global
```



# Upgrade
### Stop the controller 
### Run the database migration 
 boundary database migrate  -config /etc/boundary/controller/controller.hcl
### Start the controller

#  Logging

# Controller logging configuration
https://developer.hashicorp.com/boundary/docs/configuration/events 

```
# Events (logging) configuration. This
# configures logging for ALL events to both
# stderr and a file at /var/log/boundary/controller.log
events {
  audit_enabled       = true
  sysevents_enabled   = true
  observations_enable = true
  sink "stderr" {
    name = "all-events"
    description = "All events sent to stderr"
    event_types = ["*"]
    format = "cloudevents-json"
  }
  sink {
    name = "file-sink"
    description = "All events sent to a file"
    event_types = ["*"]
    format = "cloudevents-json"
    file {
      path = "/var/log/boundary"
      file_name = "controller.log"
    }
    audit_config {
      audit_filter_overrides {
        sensitive = "redact"
        secret    = "redact"
      }
    }
  }
```


## Authentications

  filter:  jq '.data| del(.auth.grants_info )'
### Failed
```bash
boundary  authenticate password  -addr=https://boundary.trollprod.org:9200  -scope-id=o_9oFaXugvAS  -keyring-type=none  -login-name=nicolov

Error from controller when performing authentication

Error information:
  Kind:                Unauthenticated
  Message:             Unable to authenticate.
  Status:              401
  context:             Error from controller when performing authentication
```

Two logs

GET "/v1/auth-methods?scope_id=o_9oFaXugvAS"
```json
{
  "id": "eIObvwr39S",
  "source": "https://hashicorp.com/boundary/sbg/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_2pumBTWsH9",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-11-28T14:43:30.286812638+01:00",
    "request_info": {
      "id": "gtraceid_QPtj25tKKwGcjgM25Iau",
      "method": "GET",
      "path": "/v1/auth-methods?scope_id=o_9oFaXugvAS",
      "client_ip": "192.168.8.1"
    },
    "auth": {
      "auth_token_id": "",
      "user_info": {
        "id": "u_anon"
      }
    }
  }
}
```

POST "/v1/auth-methods/ampw_oLQCiHpRoE:authenticate"

```json
  "id": "K357zKZtuS",
  "source": "https://hashicorp.com/boundary/sbg/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_MUUCbIyJ9B",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-11-28T14:43:47.78912065+01:00",
    "request_info": {
      "id": "gtraceid_aYcBV40LI5ck5EQ725fr",
      "method": "POST",
      "path": "/v1/auth-methods/ampw_oLQCiHpRoE:authenticate",
      "client_ip": "192.168.8.1"
    },
    "auth": {
      "auth_token_id": "",
      "user_info": {
        "id": "u_anon"
      },
      "grants_info": {
        "grants": [
        ]
      },
      "email": "[REDACTED]",
      "name": "[REDACTED]"
    },
    "request": {
      "details": {
        "auth_method_id": "ampw_oLQCiHpRoE",
        "Attrs": {
          "PasswordLoginAttributes": {
            "login_name": "[REDACTED]",
            "password": "[REDACTED]"
          }
        },
        "command": "login"
      }
    },
    "response": {
      "status_code": 401
    },
    "correlation_id": "4e6eef8e-0329-60f3-f404-27dac4b2dc93"
  },
  "datacontentype": "application/cloudevents",
  "time": "2024-11-28T14:43:47.789135245+01:00",
```

https://developer.hashicorp.com/boundary/docs/configuration/events/common 
Switch to

```
{
  audit_config {
  audit_filter_overrides {
    secret    = "redact"
  }
}
```

```json
{
  "id": "zYPFqedOXe",
  "source": "https://hashicorp.com/boundary/sbg/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_ZjWvnHFNnL",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-11-28T14:57:52.064986539+01:00",
    "request_info": {
      "id": "gtraceid_pskw3dCHwOcX3Xx4HFpk",
      "method": "GET",
      "path": "/v1/sessions?include_terminated=true&recursive=true&scope_id=global",
      "public_id": "at_NRLrWpKDbP",
      "client_ip": "192.168.8.1"
    },
    "auth": {
      "auth_token_id": "",
      "user_info": {
        "id": "u_OyLPs9P6BA"
      },
      "grants_info": {
        "grants": [
        ]
      },
      "email": "encrypted:ChwaPoMrh084x9148j8Jqes4d8s5rzdpw7-f1k4hKhEaD2tka3ZfeGV1ZEd2dHQ0bw",
      "name": "encrypted:Chz5ITKPVAU-ExFUbCXyKJTyjYcgoYOdA3zEq6GdKhEaD2tka3ZfeGV1ZEd2dHQ0bw"
    },
    "request": {
      "details": {
        "scope_id": "global",
        "recursive": true,
        "include_terminated": true
      }
    },
    "response": {
      "status_code": 200,
      "details": {
        "response_type": "complete",
        "sort_by": "created_time",
        "sort_dir": "desc"
      }
    },
    "correlation_id": "58846153-75c9-36ba-3905-d94b74f920a3"
  },
  "datacontentype": "application/cloudevents",
```

```hcl
{
  audit_config {
  audit_filter_overrides {
    sensitive = ""
    secret    = "redact"
  }
}
```

```json
{
  "id": "D4F8kTnxYN",
  "source": "https://hashicorp.com/boundary/sbg/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_w5jdYMpMvl",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-11-28T15:39:17.742750736+01:00",
    "request_info": {
      "id": "gtraceid_DHbnQMOOzskkfddkFwFZ",
      "method": "POST",
      "path": "/v1/auth-methods/ampw_oLQCiHpRoE:authenticate",
      "client_ip": "192.168.8.1"
    },
    "auth": {
      "auth_token_id": "",
      "user_info": {
        "id": "u_anon"
      },
      "grants_info": {
        "grants": [
        ]
      }
    },
    "request": {
      "details": {
        "auth_method_id": "ampw_oLQCiHpRoE",
        "Attrs": {
          "PasswordLoginAttributes": {
            "login_name": "nicolov",
            "password": "[REDACTED]"
          }
        },
        "command": "login"
      }
    },
    "response": {
      "status_code": 401
    },
```

```json 
{
  "id": "e_RZH2MJnh3p",
  "version": "v0.1",
  "type": "APIRequest",
  "timestamp": "2024-11-28T16:19:27.405664527+01:00",
  "request_info": {
    "id": "gtraceid_niywBoT8Twa6W7KldpPa",
    "method": "POST",
    "path": "/v1/auth-methods/ampw_oLQCiHpRoE:authenticate",
    "client_ip": "192.168.8.1"
  },
  "auth": {
    "auth_token_id": "",
    "user_info": {
      "id": "u_anon"
    }
  },
  "request": {
    "details": {
      "auth_method_id": "ampw_oLQCiHpRoE",
      "Attrs": {
        "PasswordLoginAttributes": {
          "login_name": "nicolov",
          "password": "[REDACTED]"
        }
      },
      "command": "login"
    }
  },
  "response": {
    "status_code": 200,
    "details": {
      "Attrs": {
        "AuthTokenResponse": {
          "id": "at_2y5nfd7xRm",
          "scope": {
            "id": "o_9oFaXugvAS",
            "type": "org",
            "parent_scope_id": "global"
          },
          "token": "[REDACTED]",
          "user_id": "u_OyLPs9P6BA",
          "auth_method_id": "ampw_oLQCiHpRoE",
          "account_id": "acctpw_FuAD8xu3uh",
          "created_time": {
            "seconds": 1732807167,
            "nanos": 360355000
          },
          "updated_time": {
            "seconds": 1732807167,
            "nanos": 360355000
          },
          "approximate_last_used_time": {
            "seconds": 1732807167,
            "nanos": 360355000
          },
          "expiration_time": {
            "seconds": 1733411967
          }
        }
      },
      "command": "login"
    }
  },
  "correlation_id": "721b7e32-9983-d108-050a-2fe16b90f9be"
}
```

### Success

```bash
boundary  authenticate password  -addr=https://boundaryxxx:9200  -scope-id=o_9oFaXugvAS  -keyring-type=none  -login-name=nicolov
Please enter the password (it will be hidden):

Authentication information:
  Account ID:      acctpw_FuAD8xu3uh
  Auth Method ID:  ampw_oLQCiHpRoE
  Expiration Time: Thu, 05 Dec 2024 16:19:27 CET
  User ID:         u_OyLPs9P6BA

Storing the token in a keyring was disabled. The token is:
at_2y5nfd7xRm_s1FFF3PuipGpKDRmrZ66F5TpeKXy6VqzwtaZz3BuG4YCh91PBecjQjKe28rQtZY7m38KUgaTdPuYMW2i2mKKu8yEUaKsoU1SyeUoP2JYarmDkB5Hr39QTF4
Please be sure to store it safely!
```


POST  /v1/auth-methods/ampw_oLQCiHpRoE:authenticate"
GET "/v1/auth-tokens/at_mY7go1gDnb"
GET "/v1/auth-tokens/at_NRLrWpKDbP"
GET "/v1/auth-tokens/at_2y5nfd7xRm",
GET "/v1/auth-tokens/at_mY7go1gDnb"
GET "/v1/users/u_OyLPs9P6BA:list-resolvable-aliases?list_token=BePGUvtZ6XDfLAn1jtuqBKMFAqbFJwZeKThXMxAdbD718ix4uTqyPtMpqRvQSdMm3Yvew4UU&recursive=true&scope_id=global",
GET  "v1/targets?list_token=GnJNZBNZCSLrXANdeQDiaMkjXtHEcp2xXEN4GHmBfGLbeiBga7wXFyuyQqvw4hgh4uBJTk7K3d9s&recursive=true&scope_id=global"
GET /v1/sessions?include_terminated=true&recursive=true&scope_id=global


## Authorize-session

  filter:  jq '.data| del(.auth.grants_info )'

### Failed invalid token
```bash
 boundary  targets  authorize-session -token env://BOUNDARY_TOKEN  -addr=https://boundary.trollprod.org:9200  -id=ttcp_aCcIKQRu0P
Error from controller when performing authorize-session on a session against target

Error information:
  Kind:                Unauthenticated
  Message:             Unauthenticated, or invalid token.
  Status:              401
  context:             Error from controller when performing authorize-session on a session against target
```

```json
{
  "id": "41xDt1tJK7",
  "source": "https://hashicorp.com/boundary/sbg/controller",
  "specversion": "1.0",
  "type": "audit",
  "data": {
    "id": "e_BJCQ2gSsp9",
    "version": "v0.1",
    "type": "APIRequest",
    "timestamp": "2024-11-28T17:50:22.647077926+01:00",
    "request_info": {
      "id": "gtraceid_9VeGFcKRus1p99Nv6Df8",
      "method": "POST",
      "path": "/v1/targets/ttcp_aCcIKQRu0P:authorize-session",
      "public_id": "at_SKjeqEEdYT",
      "client_ip": "192.168.8.1"
    },
    "auth": {
      "auth_token_id": "",
      "user_info": {
        "id": "u_anon"
      }
    },
    "request": {
      "details": {
        "id": "ttcp_aCcIKQRu0P"
      }
    },
    "response": {
      "status_code": 401
    },
    "correlation_id": "97b19ac5-2471-4011-35bf-1a2ce86a2487"
  },
```