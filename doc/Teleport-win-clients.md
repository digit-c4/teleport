# Teleport’s client installation on EC Corporate windows laptop

The following Teleport clients  are avaible on Windows platform
-	Web
-	Teleport Connect
-	`tsh.exe`

![Teleport overview](res/teleport_client_overview.png)


# Table of Contents
1. [Teleport User registration](#teleport-user-registration)
2. [Teleport Web](#TeleportWeb)
3. [Teleport Connnect](#TeleportConnnect)
4. [Teleport TSH](#TeleportTsh)
5. [Common issues](#TeleportIssue)


## Teleport user registration <a name="TeleportEnduser"></a>
### Register a new user

User receives an invite URL ``https://tele.xxx.tech.ec.europa.eu:3080/web/invite/36ef87aza9qgzcdd74ad40fe51efaf0a`` valid for 1 hour by default.

![Web Teleport user registration 1](res/web-user-registration-1.png)

Then choose a strong password and can enroll a second factor authentication if enabled. 

![Web Teleport user registration 2](res/web-user-registration-2.png)
![Web Teleport user registration 3](res/web-user-registration-3.png)
![Web Teleport user registration 4](res/web-user-registration-4.png)

### Change user password

To change the password, click on the user's icon on top right and then select "Account settings".

![Web Teleport change password 1](res/web-user-password-1.png)

![Web Teleport change password 2](res/web-user-password-2.png)

### Add an additional second factor

To change the password, click on the user's icon on top right and select "Account settings". Then select "Add MFA"

[Add via command line](#add-an-addtional-multifactor)

## Teleport Web <a name="TeleportWeb"></a>

### Login to a Teleport proxy
Connection the Teleport proxy public URL https://tele.xxx:3080 

![Web Teleport login 1](res/web-login-1.png)

After succesful authentication, a list of avaible nodes will be displayed in the "Ressources" tab.

![Web Teleport login 3](res/web-login-3.png)

### Logout of a Teleport proxy

Click on the user's icon in the upper right corner and the select "Logout'.

![Web Teleport login 3](res/web-logout-1.png)


### Connect to a node

Click on connect and select the login name.

![Web Teleport login 2](res/web-login-2.png)

### File transfer

When connected to a node, click on the upload/download icons in the top left of the node session.

![Web Teleport file tansfer](res/web-transfer-1.png)

### List recorded sessions

Go the "Access Management" tab, the "Session recording" list are available under "Activity".

![Web Teleport session recording](res/web-session-recording-1.png)


### Replay a session
Just click on the "PLAY" button in the session list. 
By default users can only replay their own sessions. Only users with "Audit" role have the right to replay session from other users. 

### Session sharing

![Web session share 1](res/web-session-share-1.png)

![Web session share 2](res/web-session-share-2.png)

The list of users sharing the session is display in the upper left corner.

![Web session share 3](res/web-session-share-3.png)


## Teleport Connect <a name="TeleportConnect"></a>

### Install Teleport Connect

The client is only available for Welcome’s Windows 10 workstations.
Go to "EC store" and search for "Teleport Connect"

![Teleport Connect installation](res/connect-install-1.png)

The package installs two components the `tsh.exe` CLI colland and the **Teleport Connect** terminal emulator.

Launch the **Teleport Connect** application from the start menu.

![Teleport Connect installation 2](res/connect-install-2.png)

When the application start, the user will be prompted for the EC Internet Proxy credentials.

![Teleport Connect Internet proxy](res/connect-internet-proxy-1.png)


### Login to a Teleport proxy

First connection to a Teleport cluster.

![Teleport Connect Login](res/connect-login-1.png)

Enter the URL of the of the **"Teleport Proxy** server (not to be confused with the EC internet Proxy).
By the default the "**Teleport proxy**" listen in port 3080.

![Teleport Connect Login](res/connect-login-2.png)

![Teleport Connect Login](res/connect-login-3.png)

After successful login, the list of available nodes is display on the main window.

![Teleport Connect Login](res/connect-login-4.png)

In the background a short lived certificate will be issued to the teleport user and store it under `$TELEPORT_HOME`.

### Logout of a Teleport proxy

![Teleport Connect Logout](res/connect-logout-1.png)

Teleport logout from all users will delete all references to Teleport Proxy.

### Check user's connection status

### List available nodes
List of node available to the teleport user are listed in the main windows.

### Connect to a node

![Teleport Connect login to node](res/connect-node-1.png)

![Teleport Connect login to node](res/connect-node-2.png)

Teleport Proxy enforces the connection policy and may restrict the login name used to connect to a node.

The list of allowed logins is available is usually prefilled under the connect button.
Login with other users is available by filling the connect field.

![Teleport Connect login to node](res/connect-node-3.png)


### Port redirection
Not directly avaible in "Teleport Connect", open a terminal inside "Teleport Connect" (Ctrl+Shift+T) and use the `tsh.exe` (see  [TSH port-redirection](#tsh-port-redirection)).


### File transfer
When connected to a node, click on the upload/download icons in the top left of the node session.

![Teleport Connect transfer](res/connect-transfer-1.png)

![Teleport Connect transfer](res/connect-transfer-2.png)

![Teleport Connect transfer](res/connect-transfer-3.png)

### Session sharing
Not directly avaible in "Teleport Connect", use Web or `tsh` client

### Advanced configuration
![Teleport Connect advanced option](res/connect-advanced-1.png)

```yaml
{
  "$schema": "schema_app_config.json",
  "theme": "dark",
  "usageReporting.enabled": false
}
```


## Teleport tsh <a name="TeleportTsh"></a>

### Start tsh.exe

Tsh is part of the "Teleport connect" package availble in "EC store" and is launched from the "Start Menu".

![Teleport TSH start ](res/tsh-install-1.png)

If the "Teleport Proxy" is accessed via the Internet Proxy, fill the Internet proxy credentials.

![Teleport TSH start ](res/tsh-internet-proxy-1.png)

To share session state between Teleport Connect and tsh.exe
Set the TELEPORT_HOME variable to point to the location where Teleport Connect store 

`$Env:TELEPORT_HOME = $Env:USERPROFILE + "\AppData\Roaming\Teleport Connect\tsh"`


### Login to a Teleport proxy
**```tsh logon  --proxy="Teleport.proxy.local:3080" –user="Teleport-user-name"```**

The teleport user name might be different from the user name used to connect to nodes (logins). 

    tsh.exe login  --proxy=tele.xxxx:3080  --user=snet
    Enter password for Teleport user snet:
    Using platform authenticator, follow the OS dialogs
    > Profile URL:      https://tele.xxxx:443
    Logged in as:       snet
    Cluster:            tele.xxxx
    Roles:              AWS-IPSEC
    Logins:             ec2-user
    Kubernetes:         enabled
    Valid until:        2024-04-03 02:03:09 +0200 CEST [valid for 8h0m0s]
    Extensions:         login-ip, permit-port-forwarding, permit-pty,


### Logout of a Teleport proxy

Teleport logout will delete certificates and references to all "Teleport Proxy" servers.

    tsh.exe logout
    Logged out all users from all proxies.


### Check user's connection status

Check teleport status in the **client host**

    tsh.exe status                                                                                            
    > Profile URL:      https://tele.xxxxg:443
    Logged in as:       snet
    Cluster:            tele.xxxxxx
    Roles:              AWS-IPSEC
    Logins:             ec2-user
    Kubernetes:         enabled
    Valid until:        2024-04-02 23:08:27 +0200 CEST [valid for 6h30m0s]
    Extensions:         login-ip, permit-port-forwarding, permit-pty


Check teleport status when connected to a **node**. This can usefull to get the `session id` to share the connection.

    [ec2-user@ip-10-71-0-117 ~]$ teleport status
    User ID     : snet, logged in as ec2-user from 158.169.150.27 53510 44104
    Cluster Name: tele.xxxx
    Host UUID   : fd9c430b-83a2-48d9-a659-156279a27116
    Session ID  : f66320cf-eb25-4ce2-9f3e-5d09f4319d07
    Session URL : https://tele.xxxx:443/web/cluster/tele.xxxx/console/session/f66320cf-eb25-4ce2-9f3e-5d09f4319d07

### List available nodes 

Check available nodes for the currently logged Teleport user

    tsh.exe ls
    Node Name                           Address    Labels
    ----------------------------------- ---------- --------------------------------
    ip-10-71-0-117.aws.cloud.tech.ec... ⟵ Tunnel   chassis=vm,cpe=cpe:2.3:o:amaz...
    ip-10-71-1-69.aws.cloud.tech.ec.... ⟵ Tunnel   chassis=exit status 1 output:...

Check available nodes (display only the names)

    tsh.exe ls --format names
    ip-10-71-0-117.aws.cloud.tech.ec.europa.eu
    ip-10-71-1-69.aws.cloud.tech.ec.europa.eu

### Connect to a node
tsh ssh takes the same arguments as the OpenSSH cli
**tsh ssh <username>@<nodename>**

    tsh ssh ec2-user@ip-10-71-0-117.aws.cloud.tech.ec.europa.eu

    [ec2-user@ip-10-71-0-117 ~]$ w
    14:46:19 up 20:04,  1 user,  load average: 0.02, 0.01, 0.00
    USER     TTY        LOGIN@   IDLE   JCPU   PCPU WHAT
    ec2-user pts/0     14:44    0.00s  0.01s  0.00s w

The list of avaible login names for a telport username are listed in the `tsh status` output 

### Port redirection {#tsh-port-redirection}

If port redirection is allowed by a role assigned to Teleport user

    tsh ssh -L 8080:127.0.0.1:80 ec2-user@ip-10-71-0-117.aws.cloud.tech.ec.europa.eu

The output of **`tsh status`**, port forwding will be listed in the Extensions 

    tsh status
    > Profile URL:      https://tele.xxxx:443
    Logged in as:       snet
    Cluster:            tele.xxxx
    Roles:              AWS-IPSEC
    Logins:             ec2-user
    Kubernetes:         enabled
    Valid until:        2024-04-02 23:08:27 +0200 CEST [valid for 5h10m0s]
    Extensions:         login-ip, permit-port-forwarding, permit-pty


### File transfer

User `tsh scp` to transfert files between host and nodes 

    tsh.exe scp .\archive.tar.gz ec2-user@ip-10-71-0-117.aws.cloud.tech.ec.europa.eu:/tmp

    archive.tar.gz 100% |█████████████████████████| (4.3/4.3 kB, 2.3 MB/s)

### Session sharing

List the active session

    tsh.exe sessions ls
    ID                                   Kind Created              Hostname                                   Address                              Login    Command
    ------------------------------------ ---- -------------------- ------------------------------------------ ------------------------------------ -------- -------
    a05acb4d-890c-4dfe-966d-937f3c9f2cea ssh  2024-04-05T08:28:35Z ip-10-71-0-117.aws.cloud.tech.ec.europa.eu fd9c430b-83a2-48d9-a659-156279a27116 ec2-user

Join an active session

    tsh.exe join --mode=peer a05acb4d-890c-4dfe-966d-937f3c9f2cea
    
If allowed by the role policy a session can be joind as **read-only** with `--mode==observer` or **read-write** with the option `--mode==observer`

### Aliases

Create the config.yaml in the directory "config" under  "TELEPORT_HOME"

Common TELEPORT_HOME locations

- C:\Users\%USERNAME%\AppData\Roaming\Teleport Connect\tsh\
- C:\Users\%USERNAME%\.tsh\

Create the alias "nodes" that list the nodes in "name" format

```yaml
aliases:
  "nodes": "tsh.exe ls --format=names"
```


Then run : `tsh "alias"`

    tsh.exe nodes
    ip-10-71-0-117.aws.cloud.tech.ec.europa.eu
    ip-10-71-1-69.aws.cloud.tech.ec.europa.eu



### Add an addtional multifactor

    tsh mfa add
    Choose device type [TOTP, WEBAUTHN]: TOTP
    Enter device name: mobile2
    Using platform authentication for *registered* device, follow the OS dialogs

    Open your TOTP app and scan the QR code. Alternatively, you can manually enter these fields:
    URL: otpauth://totp/tele.xxxxx:snet@tele.xxxxxx?algorithm=SHA1&digits=6&issuer=tele.xxxx
    &period=30&secret=VP62A64M711SYZKWJTWQ5BY
    Account name: snet@tele.xxxxx
    Secret key: VP62A64M711SYZKWJTWQ5BY
    Issuer: tele.xxxxx
    Algorithm: SHA1
    Number of digits: 6
    Period: 30s

    Once created, enter an OTP code generated by the app:
    MFA device "mobile2" added.


## Common issues <a name="TeleportIssues"></a>

### Certificate has expired
  
  The temporary certificate has expired, you need to login again to get a new [short-lived certificates](https://goteleport.com/learn/what-are-short-lived-certificates/). 
  

    ```bash
    tsh.exe sessions ls
    ERROR: ssh: cert has expired
    ```
- **Cause** : The certificate issued by Teleport has expired.
- **Resolution**: Login again with `tsh login`.

### Login not allowed on node

![Teleport Connect login to node](res/connect-node-4.png)

- **Cause** : "teleport proxy" policy does not allow to connect to a node with the specified account.
- **Resolution**: Use an account allowed ( Allowed account can be found in `tsh status` output).


### Repeated Teleport authentication failures
Teleport authentication always fails, even if the password is the correct one.

- **Cause**: The proxy credentials might be wrong and prevent access to the "Teleport Proxy".
- **Resolution**: Restart the application from the start menu and double the proxy credentials.

### Incompatible versions

    WARNING
    Detected potentially incompatible client and server versions.
    Minimum client version supported by the server is 13.0.0 but you are using 12.4.18.
    Please upgrade tsh to 13.0.0 or newer or use the --skip-version-check flag to bypass this check.
    Future versions of tsh will fail when incompatible versions are detected.

- **Cause**: Compatibilty between Teleport clients and server.
- **Resolution**: Compatibility is only garanteed between the same major version. Use option `--skip-version-check` to force connection until a new package is available in EC Store.