# Teleport proxy (server)

[Teleport official doc](https://goteleport.com/docs/)


# User account management

### List users 
```bash
tctl users ls
User           Roles
-------------- ---------------------
bot-bot-tctl   bot-bot-tctl
bot-robot      bot-robot
nicolov        AWS-IPSEC
niol           AWS-IPSEC,TP
teleport-admin access,auditor,editor
```

### Create users

```bash
tctl  users add pierpbe --roles=AWS-IPSEC  --logins=ec2-user,snet --ttl 48h
User "pierpbe" has been created but requires a password. Share this URL with the user to complete user setup, link is valid for 48h:
https://tele.xxx:443/web/invite/0c8bff3762a9897fb

NOTE: Make sure tele.xxxx:443 points at a Teleport proxy which users can access.
```
The user must has one hour to connect and activate the account within the specified TTL (default 1h, max 48h).
If the user fails to activate the account a new token can be create be the `tcl users reset` command.


```bash
tctl users reset pierpbe --ttl 24h
User "pierpbe" has been reset. Share this URL with the user to complete password reset, link is valid for 24h:
https://tele.xxx:443/web/reset/005e44199f2471c8dc7a5c3ced18bfc4

NOTE: Make sure tele.xxx:443 points at a Teleport proxy which users can access.
```


### Assign user to roles
```bash
tctl users update nicolov --set-roles  AWS-IPSEC
User nicolov has been updated:
        New roles: AWS-IPSEC
```

### Assign SSH logins
To allow a user to login as a specific user on a target node. The login list **replace** current one.
```bash
tctl users update pierpbe  --set-logins ec-user,snet,root
User pierpbe has been updated:
        New logins: ec-user,snet,root
```

### Set MFA
User must login to set a MFA !!
```bash
tsh  mfa add --type TOTP --name "default OTP" --user pierpbe
Enter password for Teleport user pierpbe:
ERROR: failed reading prompt response
        context canceled
```

### Reset credentials
```bash
 tctl users reset pierpbe
User "pierpbe" has been reset. Share this URL with the user to complete password reset, link is valid for 8h:
https://tele.XXXX:443/web/reset/6b6149056da07f9c9b6b4fab071d29d6

NOTE: Make sure tele.XXX:443 points at a Teleport proxy which users can access.
```


# Roles management 
### Create
```bash
tctl  create -f snet-users-role.yaml
role "snet-access" has been created
```
### Update
To update a role run the create again with update 
```bash
tctl create -f roles.yaml
```
### Delete
```bash
tctl rm  roles/snet-access
role "snet-access" has been deleted
```

# Admin accounts
## Initial account creation 

## Generate initial password and OTP secrets

To create first accounts a password and an OTP seed must be generated
Run the script ```gen_init_creds.sh``` to generate Teleport local password and TOTP secret
```bash
./tests/gen_init_creds.sh
PASS: 1555ae2b1dfeb3e92279e07c0dcd946bcb87af9e3d0e200c4d9fba89e0718c6c
TELEPORT_PASS: JDJ5JDEwJHVNeFdzbXVOQ29iR2xlY2UwQnhGUnVubFBlSG1mV3dHa0gzaUdoY2RRT0E3MjVxenN4NnhlCg==
OTP: QPANVXZ3QB5ZTA5MJ4MRFRV4QGHD6AYG
```


### Password 

Bcrypt password can generate by `htpasswd` from  package `apache2-utils` (apt-get install apache2-utils )

```bash
PASS=PASS=`openssl rand -hex 3`
htpasswd  -nb -B -C 10  user ${PASS}| head -1 | cut -f 2 -d ":"| base64 -w 0
```
pass: fe145f90ebba8f3b87e9eb31181f9bbaefd1b91adaa59b2d1c0f2c77aae693a3

bcrypt with a cost of 10: $2y$10$uYjHKGxPdx8yiyksyn.Q/O9FNuCS1L/TzBgHWrNBvA.KUQ3a5Oyne

base64 bcrypt with a cost of 10 : JDJ5JDEwJHVZakhLR3hQZHg4eWl5a3N5bi5RL085Rk51Q1MxTC9UekJnSFdyTkJ2QS5LVVEzYTVPeW5lCg==



###  OTP secret
Generate [RFC 6238](https://datatracker.ietf.org/doc/html/rfc6238) TOTP secret 

```bash
openssl rand 20 | base32
QZT4HQT7BS6GO6NU2O2XGGXOKJSLR7WR
```




# Login failure rules
A local user is blocked from attempting logins if, within a 30 minute window, a local user has multiple:

- failed login attempts or
- failed password resets

The block lasts 30 minutes. After the block has expired the user may attempt to log in again.



```yaml
tctl get users/pierpbe
kind: user
metadata:
  id: 1713452371723615753
  name: pierpbe
  revision: dfbd79af-1bf3-4e27-b9d1-5ada5629a2dd

status:
    is_locked: true
    lock_expires: "2024-04-18T15:29:31.722940571Z"
    locked_message: user has exceeded maximum failed login attempts
    locked_time: "2024-04-18T14:59:31.722946779Z"
```


# Manage invite tokens

### List active tokens
```bash
tctl tokens ls
Token                            Type   Labels Expiry Time (UTC)
-------------------------------- ------ ------ -------------------------------
57827cc14bea22782c8cc8437920666c Signup        19 Apr 24 14:20 UTC (23h38m58s)
```

### Remove token
```bash
tctl tokens rm 57827cc14bea22782c8cc8437920666c
Token 57827cc14bea22782c8cc8437920666c has been deleted
```


# Node management

## Add node
```bash
The invite token:9bbcc9...720cb1117
This token will expire in 30 minutes.

Run this on the new node to join the cluster:

> teleport start \
   --roles=node \
   --token=9bbcc9...720cb1117 \
   --ca-pin=sha256:76ebe770ffed15febba6237d9156eafd624ebeb44ef510f9428909e1173076e5 \
   --auth-server=https://tele.XXXX:443/
```

## List nodes
```bash
tctl nodes ls
Host                                       UUID                                 Public Address   Labels                                                                          Version
------------------------------------------ ------------------------------------ ---------------- ------------------------------------------------------------------------------- -------
ip-10-71-0-80.aws.cloud.tech.ec.europa.eu  307e4acc-4265-4908-b0d9-de5c2691780f                  chassis=vm,cpe=cpe:2.3:o:amazon:amazon_linux:2,distrib=Amazon Linux 2,enviro... 15.4.6
192.168.8.1                                43c37960-3177-4a1c-9ad9-7681f72325a1 192.168.8.1:3022 chassis=vm,cpe=cpe:/o:opensuse:leap:15.6,distrib=openSUSE Leap 15.6,env=Tele... 15.4.6
ip-10-71-1-69.aws.cloud.tech.ec.europa.eu  52de6a42-a326-48ca-b89c-b8783d0cc19f                  chassis=vm,cpe=cpe:2.3:o:amazon:amazon_linux:2,distrib=Amazon Linux 2,enviro... 15.4.6
blackened                                  a7076fd1-85b7-4e78-a33d-050dcb3ae21e                  chassis=vm,cpe=cpe:/o:opensuse:leap:15.5,distrib=openSUSE Leap 15.5,environm... 15.4.4
ip-10-71-0-117.aws.cloud.tech.ec.europa.eu fd9c430b-83a2-48d9-a659-156279a27116                  chassis=vm,cpe=cpe:2.3:o:amazon:amazon_linux:2023,distrib=Amazon Linux 2023.... 15.4.5
```

## List all inventory (including proxy, auth, windows desktop gateway)
```bash
tctl inventory list
Server ID                            Hostname                                   Services                       Agent Version Upgrader Upgrader Version
------------------------------------ ------------------------------------------ ------------------------------ ------------- -------- ----------------
307e4acc-4265-4908-b0d9-de5c2691780f ip-10-71-0-80.aws.cloud.tech.ec.europa.eu  Node                           v15.4.6       none     none
43c37960-3177-4a1c-9ad9-7681f72325a1 192.168.8.1                                Auth,Node,Proxy,WindowsDesktop v15.4.6       none     none
52de6a42-a326-48ca-b89c-b8783d0cc19f ip-10-71-1-69.aws.cloud.tech.ec.europa.eu  Node                           v15.4.6       none     none
a7076fd1-85b7-4e78-a33d-050dcb3ae21e vm                                         Node                           v15.4.6       none     none
fd9c430b-83a2-48d9-a659-156279a27116 ip-10-71-0-117.aws.cloud.tech.ec.europa.eu Node                           v15.4.6       none     none

```


# Backup & Restore

## Backup
### Teleport Nodes
Teleport's Nodes are stateless. 
Only `/etc/teleport.yaml` must be backed up.

### Teleport Proxy
Teleport's Proxy Service is stateless. 
Only `/etc/teleport.yaml`  and TLS certificates must be backed up.


### Auth Service
Back up the `/var/lib/teleport` directory and the output of `tctl get all --with-secrets`.

```bash
tctl get all --with-secrets  > backup-all-`date "+%Y%m%d"`.yml
```

## Restore

Start a new instance and bootstrap it with the latest backup. (If needed restore the Proxy TLS certificates)
```bash
teleport start --config /etc/fresh-teleport.yaml --bootstrap backup-all-20240708.yml
```
This will restore the configuration but not the recording history. To restore the recording history, the `data_dir` directory must be restored.

```bash
2024-07-08T14:00:30+02:00 INFO  Starting Teleport with a config file version:15.4.7 config_file:/etc/teleport-restore.yaml common/teleport.go:724
2024-07-08T14:00:30+02:00 INFO [PROC:1]    Generating new host UUID pid:13186.1 host_uuid:8c6bded0-59e8-415a-866f-80909c6505d4 service/service.go:6274
2024-07-08T14:00:31+02:00 INFO [PROC:1]    Service is creating new listener. pid:13186.1 type:diag address:192.168.8.1:9245 service/signals.go:249
2024-07-08T14:00:31+02:00 INFO [DIAG:1]    Starting diagnostic service. pid:13186.1 listen_address:192.168.8.1:9245 service/service.go:3402
2024-07-08T14:00:31+02:00 INFO [PROC:1]    Service is creating new listener. pid:13186.1 type:debug address:/var/lib/teleport-restore/debug.sock service/signals.go:249
2024-07-08T14:00:32+02:00 INFO [AUTH]      Applying 31 bootstrap resources (first initialization) auth/init.go:355
2024-07-08T14:00:32+02:00 INFO [AUTH]      Updating cluster networking configuration: Kind:"cluster_networking_config" Version:"v2" Metadata:<Name:"cluster-networking-config" Namespace:"default" Labels:<key:"teleport.dev/origin" value:"config-file" > > Spec:<KeepAliveInterval:300000000000 KeepAliveCountMax:3 ProxyListenerMode:Multiplex TunnelStrategy:<AgentMesh:<> > AssistCommandExecutionWorkers:30 > . auth/init.go:783
2024-07-08T14:00:32+02:00 INFO [AUTH]      Creating session recording config: Kind:"session_recording_config" Version:"v2" Metadata:<Name:"session-recording-config" Namespace:"default" Labels:<key:"teleport.dev/origin" value:"defaults" > > Spec:<Mode:"node" ProxyChecksHostKeys:"\010\001" > . auth/init.go:814
2024-07-08T14:00:32+02:00 INFO [AUTH]      Updating cluster configuration: StaticTokens([]). auth/init.go:442
2024-07-08T14:00:32+02:00 INFO [AUTH]      Creating namespace: "default". auth/init.go:449
2024-07-08T14:00:32+02:00 INFO [AUTH]      Starting migration 1 create_db_cas migration/migration.go:123
2024-07-08T14:00:32+02:00 INFO [AUTH]      Completed migration 1 create_db_cas migration/migration.go:143
2024-07-08T14:00:32+02:00 INFO [AUTH]      Auth server is running periodic operations. auth/init.go:555
2024-07-08T14:00:32+02:00 INFO [CA]        Generating TLS certificate 1.3.9999.1.7=#131274656c652e74726f6c6c70726f642e6f7267,CN=8c6bded0-59e8-415a-866f-80909c6505d4.tele.xxx.xxx,O=Admin,POSTALCODE=null,STREET= dns_names:[192.168.8.1 *.teleport.cluster.local teleport.cluster.local] key_usage:5 not_after:2034-07-06 12:00:32.574565447 +0000 UTC tlsca/ca.go:1200
2024-07-08T14:00:32+02:00 INFO [PROC:1]    Successfully obtained credentials to connect to the cluster. pid:13186.1 identity:Admin service/connect.go:524
2024-07-08T14:00:32+02:00 INFO [PROC:1]    The process successfully wrote the credentials and state to the disk. pid:13186.1 identity:Admin service/connect.go:559
2024-07-08T14:00:32+02:00 INFO [AUTH:COMP] upload completer will run every 5m0s events/complete.go:161
2024-07-08T14:00:32+02:00 INFO [AUTH:1:CA] Cache "auth" first init succeeded. cache/cache.go:1025
2024-07-08T14:00:32+02:00 INFO [PROC:1]    Service is creating new listener. pid:13186.1 type:auth address:192.168.8.1:3025 service/signals.go:249
2024-07-08T14:00:35+02:00 INFO [WINDOWS_D] Starting Windows desktop service. pid:13186.1 listen_address:192.168.8.1:3389 service/desktop.go:252
2024-07-08T14:00:35+02:00 INFO [NODE:1:CA] Cache "node" first init succeeded. cache/cache.go:1025
2024-07-08T14:00:35+02:00 WARN [NODE:1]    Login UID is set, but it shouldn't be. Incorrect login UID breaks session ID when using auditd. Please make sure that Teleport runs as a daemon and any parent process doesn't set the login UID. pid:13186.1 service/service.go:2777
2024-07-08T14:00:35+02:00 INFO [PROC:1]    Service is creating new listener. pid:13186.1 type:node address:0.0.0.0:3022 service/signals.go:249
2024-07-08T14:00:35+02:00 INFO [NODE:1]    SSH Service is starting. pid:13186.1 version:15.4.7 git_ref:api/v15.4.7-5-g918037cedf listen_address:0.0.0.0:3022 cache_policy:in-memory cache service/service.go:2908
2024-07-08T14:00:35+02:00 INFO [PROC:1]    The new service has started successfully. Starting syncing rotation status. pid:13186.1 max_retry_period:1m30s service/connect.go:693
```

